<?php

use App\Models\Man;
use App\Models\User;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateFavoritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favorites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("post_id")->unsigned();
            $table->bigInteger("user_id")->unsigned();
            $table->foreign("post_id")
                ->references('id')
                ->on('posts')
                ->onDelete('cascade');
            $table->foreign("user_id")
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->default(101);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favorites');
    }
}

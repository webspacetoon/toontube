<?php

use App\Models\Category;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("title");
            $table->string("body");
            $table->bigInteger("category_id")->unsigned();
            $table->bigInteger("user_id")->unsigned();
            // $table->string('img', 255)->nullable();
            $table->foreign("category_id")
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');
            $table->string('image', 255)->nullable();
            $table->foreign("user_id")
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->default(101);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}

<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class="nav-item nav-dropdown">
<a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-tablet"></i><b><i>Tables</i></b></a>
<ul class="nav-dropdown-items">
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('post') }}'><i class='nav-icon  la la-envelope'></i> Posts</a></li>
<li class='nav-item '><a class='nav-link' href='{{ backpack_url('user') }}'><i class='nav-icon  la la la-users'></i> Users</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('comment') }}'><i class='nav-icon la la-comment'></i> Comments</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'><i class='nav-icon  la la-certificate'></i> Categories</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('favorite') }}'><i class='nav-icon  la la-heart-o'></i> Favorites</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('rate') }}'><i class='nav-icon la  la-bar-chart'></i> Rates</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('video') }}'><i class='nav-icon  la la-video-camera'></i> Videos</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('serie') }}'><i class='nav-icon  la la-forward'></i> Series</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('type') }}'><i class='nav-icon la  la-flag'></i> Types</a></li>

</ul>
</li>



<?php


use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'), // (found , default(not found)) 
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () {
    Route::crud('post', 'PostCrudController');
    Route::crud('comment', 'CommentCrudController');
    Route::crud('category', 'CategoryCrudController');
    Route::crud('favorite', 'FavoriteCrudController');
    Route::crud('rate', 'RateCrudController');
    Route::crud('video', 'VideoCrudController');
    Route::crud('serie', 'SerieCrudController');
    Route::crud('user', 'UserCrudController');
    Route::crud('type', 'TypeCrudController');
});

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\VideoController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\PostController;
use App\Http\Controllers\Api\CommentController;
use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Api\RateController;
use App\Http\Controllers\Api\FavoriteController;

Route::post('/login', [LoginController::class, 'login'])->middleware('guest');

Route::post('storepost', [PostController::class, 'store']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get("/video", [VideoController::class, 'getVideoById']);
    Route::get("/videobyseries", [VideoController::class, 'getVideosBySeries']);
    Route::resource('category', CategoryController::class);

    Route::resource('post', PostController::class);
    Route::get('PostsById', [PostController::class, 'PostsById']);
    Route::get('PostsByCategory/{id}/{post_id}', [PostController::class, 'PostsByCategory']);
    Route::get('posts/rates', [PostController::class, 'avgRate']);
    Route::get('posts/rates/{id}', [PostController::class, 'avgRatePost']);

    Route::get('search/{title}', [PostController::class, 'searchByTitle']);
  


    Route::get('rates', [RateController::class, 'index']);
    Route::get('rates/{id}', [RateController::class, 'show']);
    Route::post('rates', [RateController::class, 'store']);
    Route::put('rates/{id}', [RateController::class, 'updatee']);
    Route::delete('rates/{id}', [RateController::class, 'destroy']);

    Route::get('favorites', [FavoriteController::class, 'index']);
    Route::get('favorites/{id}', [FavoriteController::class, 'show']);
    Route::get('favorites/user/{d}', [FavoriteController::class, 'showFavorites']);
    Route::post('favorites', [FavoriteController::class, 'store']);

    Route::get('commentsbypost', [CommentController::class, 'commentsBypostId']);
    Route::resource('comment', CommentController::class);
});
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

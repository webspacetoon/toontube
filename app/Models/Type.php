<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use CrudTrait, HasFactory;

    protected $table = 'types';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    function user()
    {
        return $this->hasMany(\App\Models\User::class);
    }
}

<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

use Intervention\Image\ImageManagerStatic as Image;

class Post extends Model
{
    use CrudTrait, HasFactory;

    protected $fillable = [];
    protected $table = 'posts';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    function comment()
    {
        return $this->hasMany(\App\Models\Comment::class);
    }

    function category()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($obj) {
            Storage::delete(Str::replaceFirst(
                'storage/',
                'public/',
                $obj->image
            ));
        });
    }

    public function setImageAttribute($value)
    {
        $attribute_name = "Image";
        $disk = config('backpack.base.root_disk_name');
        $destination_path = "public/uploads/folder_1/folder_2";

        if ($value == null) {

            Storage::disk($disk)->delete($this->{$attribute_name});

            $this->attributes[$attribute_name] = null;
        }

        if (Str::startsWith($value, 'data:image')) {

            $image = Image::make($value)->encode('jpg', 90);

            $filename = md5($value . time()) . '.jpg';

            Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());

            Storage::disk($disk)->delete($this->{$attribute_name});

            $public_destination_path = Str::replaceFirst('public/', '', $destination_path);
            $this->attributes[$attribute_name] = $public_destination_path . '/' . $filename;
        } else {
            return $this->attributes[$attribute_name] = $value;
        }
    }

    public function ratings()
    {
        return $this->hasMany(Rate::class);
    }
}

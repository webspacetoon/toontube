<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Video extends Model
{
    use CrudTrait, HasFactory;

    protected $table = 'videos';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    public function series()
    {
        return $this->belongsTo(\App\Models\serie::class);
    }
}

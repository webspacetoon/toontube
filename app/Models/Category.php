<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use CrudTrait;

    use HasFactory;

    protected $table      = 'categories';
    protected $primaryKey = 'id';
    protected $guarded    = ['id'];

    function post()
    {
        return $this->hasMany(\App\Models\Post::class);
    }

    function series()
    {
        return $this->hasMany(\App\Models\Serie::class);
    }
}

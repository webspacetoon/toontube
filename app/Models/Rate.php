<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    use CrudTrait, HasFactory;

    protected $table = 'rates';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable = ['post_id', 'user_id', 'rate'];
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    function post()
    {
        return $this->belongsTo(\App\Models\Post::class);
    }
}

<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Favorite extends Model
{
    use CrudTrait, HasFactory;

    protected $table      = 'favorites';
    protected $primaryKey = 'id';
    protected $guarded    = ['id'];

    function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    function post()
    {
        return $this->belongsTo(\App\Models\Post::class);
    }
}

<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Serie extends Model
{
    use CrudTrait, HasFactory;

    protected $table = 'series';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    public function video()
    {
        return $this->hasMany(Video::class);
    }
}

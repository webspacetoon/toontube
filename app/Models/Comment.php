<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use CrudTrait, HasFactory;

    protected $table      = 'comments';
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'post_id',
        'body',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    function post()
    {
        return $this->belongsTo(\App\Models\Post::class);
    }
    
    function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}

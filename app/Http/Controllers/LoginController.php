<?php

namespace App\Http\Controllers;

use App\Models\User;

use Illuminate\Http\Request;
use App\Http\Requests\loginRequest;

class LoginController extends Controller
{
    public function login(loginRequest $request)
    {
        $fields  = $request->validated();

        // $fields = $request->validate([
        //     'sid' => 'required|string',
        //     'fcm' => 'required|string',
        // ]);

        $user = User::where('sid', $request['sid'])->first();

        if ($user) {

            $token = $user->createToken('myToken')->plainTextToken;

            $response = [
                'token' => $token,
                "result" => "The User Was Logged in Successfully"
            ];

            return response($response, 201);
        } else {

            $user = User::create([
                'sid' => $request['sid'],
                'fcm' => $request['fcm'],
            ]);

            $token = $user->createToken('myToken')->plainTextToken;

            $response = [
                'token'  => $token,
                "result" => "The User Was Registered Successfully"
            ];

            return response($response, 201);
        }
    }
}

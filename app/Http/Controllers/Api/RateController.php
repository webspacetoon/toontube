<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests\RatesRequest;
use App\Models\Rate;
use App\Http\Resources\Api\rateCollection;

use App\Http\Controllers\Api\BaseController;

class RateController extends BaseController
{
    public function index()
    {
        // return Rate::all();
        return new rateCollection(Rate::all());
    }


    public function create()
    {
    }

    public function store(RatesRequest $request)
    {
        $rate = Rate::where('user_id', $request->user_id)->where(
            'post_id',
            $request->post_id
        )->get();

        // return Rate::findOrFail($rate);

        if ($rate->isEmpty()) {
            $request->validated();
            return rate::create($request->all());
        } else {
          //  dd($rate);
            $rate = Rate::findOrFail($rate[0]->id);
            $rate->update($request->all());
            return $rate;
        }
    }

    public function show($id)
    {
        return Rate::findOrFail($id);
    }

    public function updatee(Request $request, $id)
    {
        $rate = Rate::find($id);
        $rate->update($request->all());
        return $rate;
    }

    public function destroy($id)
    {
        Rate::findOrFail($id)->destroy($id);;
    }
}

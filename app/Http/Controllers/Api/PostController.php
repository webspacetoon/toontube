<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PostByIdRequest;
use App\Http\Resources\Api\Post as PostResource;
use App\Http\Controllers\Api\BaseController;

use App\Models\Post;

class PostController extends BaseController
{
    public function index()
    {
        $posts = Post::all();
        return $this->handleResponse(
            PostResource::collection($posts),
            'Posts have been retrieved!'
        );
    }

    public function PostsByCategory($category_id, $post_id)
    {
        $posts = Post::where('category_id', $category_id)
            ->where('id', '>=', $post_id)->take(3)->get();


        // return Post::findOrFail($posts);
        return $this->handleResponse(
            PostResource::collection($posts),
            'Posts have been retrieved!'
        );
    }

    public function PostsById(PostByIdRequest $request)
    {
        $fields  = $request->validated();

        // $fields = $request->validate([
        //     'id' => 'required'
        // ]);

        // return Post::findOrFail($request->id);

        $post = Post::where('id', $fields['id'])->get();

        if ($post->first()) {
            $response = [
                'post'   => $post,
                "result" => "The post  returned successfully",
            ];

            return response($response, 201);
        } else {
            $response = [
                "result" => "the post does not exist"
            ];

            return response($response, 201);
        }
    }

    public function searchByTitle($title)
    {
        $posts = post::where('title', 'like', '%' . $title . '%')->get();

        return $this->handleResponse(
            PostResource::collection($posts),
            'Posts have been retrieved!'
        );
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'title'       => 'required',
            'body'        => 'required',
            'img'         => 'required',
            'category_id' => 'required',
        ]);

        //    $input['user_id'] = auth()->user()->id;

        if ($validator->fails()) {
            return $this->handleError($validator->errors());
        }

        $post = Post::create($input);

        return $this->handleResponse(new PostResource($post), 'Post created!');
    }

    public function destroy($id)
    {
        $post = Post::findOrFail($id)->destroy($id);
        return $this->handleResponse([], 'post deleted!');
        // if ($post) {
        //     $post->delete();
        //     return $this->handleResponse([], 'post deleted!');
        // } else {
        //     return $this->handleError('post not found');
        // }
    }

    public function avgRate()
    {
        $post = Post::select('posts.*', DB::raw('AVG(rates.rate) As rate'))
            ->Join('rates', 'rates.post_id', '=', 'posts.id')
            ->groupBy('posts.id')
            ->get();

        if ($post->first()) {
            $response = [
                'post'   => $post,
                "result" => "The post  returned successfully",
            ];
            return response($response, 201);
        } else {
            $response = [
                "result" => "the post does not exist"
            ];
            return response($response, 201);
        }


        /*function getMag()
    {
        $data = DB::table('Magasins')
            ->join('Produits', 'Produits.id', '=', 'Magasins.produit_id')
            ->select('Magasins.id', 'Produits.produit', 'Produits.modele', 'Magasins.code_article', 'Magasins.article', 'Magasins.qte', 'Magasins.qte_ret', 'Magasins.created_at')
            ->get();
        return DataTables::of($data)
        ->addColumn('action', function($mag){
            $ldate = date('Y-m-d');
            $mg = DB::table('Magasins')
            ->select(DB::raw("DATE(created_at)"))->get();
            if($mg = $ldate)
            {
                return '<a href="#" class="btn btn-xs btn-primary Ajouter_une_quantite" id="'.$mag->id.'"><i class="glyphicon glyphicon-plus"></i> Ajouter une quantité</a>';
            }
        })
        ->make(true);
    }
         */
    }

    public function avgRatePost($id)
    {
        $post = Post::select('posts.*', DB::raw('AVG(rates.rate) As rate'))
            ->Join('rates', 'rates.post_id', '=', 'posts.id')
            ->where('posts.id', '=', $id)
            ->groupBy('posts.id')
            ->get();

        if ($post->first()) {
            $response = [
                'post'   => $post,
                "result" => "The post  returned successfully",
            ];
            return response($response, 201);
        } else {
            $response = [
                "result" => "the post does not exist"
            ];
            return response($response, 201);
        }
    }
}

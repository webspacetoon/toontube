<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Models\Favorite;
use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\FavoritesRequest;

class FavoriteController extends BaseController
{

    public function index()
    {
        $favorite = Favorite::all();
        if ($favorite->first()) {
            $response = [
                'favorites' => $favorite,
                "result"    => "The favorite  returned successfully",
            ];
            return response($response, 201);
        } else {
            $response = [
                "result" => "the favorite does not exist"
            ];
            return response($response, 201);
        }
    }

    public function store(FavoritesRequest $request)
    {
        $favorite = Favorite::where(
            'user_id',
            $request->user_id
        )->where(
            'post_id',
            $request->post_id
        )->get();

        if ($favorite->isEmpty()) {
            $request->validated();
            // $request->validate(
            //     [
            //         'user_id' => 'required',
            //         'post_id' => 'required'
            //     ]
            // );
            $favorite = favorite::create($request->all());
            $response = [
                'favorites' => $favorite,
                "result"    => "post has been added to favorites",
            ];
            return response($response, 201);
        } else {
            favorite::destroy($favorite[0]->id);
            
            $response = [
                "result" => "post has been removed from favorites"
            ];
            return response($response, 201);
        }
    }

    public function show($id)
    {
        return Favorite::findOrFail($id);
    }

    public function showFavorites($id)
    {
        $favorite = Favorite::select('favorites.user_id', 'favorites.post_id', 'posts.title')
            ->Join('posts', 'posts.id', '=', 'favorites.post_id')
            ->where('favorites.user_id', '=', $id)
            ->orderBy('favorites.post_id')
            ->get();

        if ($favorite->first()) {
            $response = [
                'favorites' => $favorite,
                "result"    => "The favorite  returned successfully",
            ];
            return response($response, 201);
        } else {
            $response = [
                "result" => "the favorite does not exist"
            ];
            return response($response, 201);
        }
    }
}

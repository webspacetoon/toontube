<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Api\BaseController;
use App\Models\Comment;
use App\Http\Resources\Api\Comment as CommentResource;
use App\Http\Requests\CommentByIdRequest;

class CommentController extends BaseController
{
    public function commentsBypostId(CommentByIdRequest $request)
    {
        $fields  = $request->validated();
        // $fields = $request->validate([
        //     'post_id' => 'required',
        // ]);

        $comments =  Comment::where('post_id', $fields['post_id'])->get();

        if ($comments->first()) {

            $response = [
                'comments' => $comments,
                "message"  => "The comments returned successfully"
            ];
            return $this->handleResponse(
                CommentResource::collection($comments),
                $response['message']
            );
        } else {
            $response = [
                "message" => "No comments yet"
            ];
            return $this->handleResponse(
                CommentResource::collection($comments),
                $response['message']
            );
        }
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'body'    => 'required',
            'post_id' => 'required'
        ]);
        $input['user_id'] = auth()->user()->id;
        if ($validator->fails()) {
            return $this->handleError($validator->errors());
        }
        $comment = Comment::create($input);
        return $this->handleResponse(
            new CommentResource($comment), 
            'comment created!'
        );
    }

    public function update(Request $request, $id)
    {
        $Comment = Comment::findOrFail($id);
        if ($Comment) {
            $Comment->update($request->all());
            return $Comment;
        } else {
            return $this->handleError('comment not found');
        }
    }
    public function destroy($id)
    {
        $Comment = Comment::findOrFail($id)->destroy($id);
        if ($Comment) {
            $Comment->delete();
            return $this->handleResponse([], 'Comment deleted!');
        } else {
            return $this->handleError('comment not found');
        }
    }
}

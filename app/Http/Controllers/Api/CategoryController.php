<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController;
use App\Http\Resources\Api\Category as CategoryResource;

use App\Models\Category as ModelsCategory;

class CategoryController extends BaseController
{
    public function index()
    {
        $categories = ModelsCategory::all();
        return $this->handleResponse(
            CategoryResource::collection($categories),
            'categories have been retrieved!'
        );
    }
}

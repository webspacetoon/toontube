<?php

namespace App\Http\Controllers;

use App\Http\Requests\VideoByIdRequest as RequestsVideoByIdRequest;
use App\Models\Video;
use App\Requests\VideoByIdRequest;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function getVideoById(RequestsVideoByIdRequest $request)
    {
        $fields  = $request->validated();
        // $fields = $request->validate([
        //     'id' => 'required|integer',
        // ]);

        $video =  Video::findOrFail($fields['id']);

        if ($video) {

            $response = [
                'video'  => $video,
                "result" => "The video returned successfully"
            ];

            return response($response, 201);

        } else {
            $response = [

                "result" => "The video id not exist"
            ];

            return response($response, 201);

        }
    }

    public function getVideosBySeries(Request $request)
    {

        $fields = $request->validate([
            'id' => 'required|integer',
        ]);

        $video =  Video::where('series_id', $fields['id'])->get();

        if ($video->first()) {

            $response = [
                'video'  => $video,
                "result" => "The videos of the series returned successfully"
            ];

            return response($response, 201);

        } else {
            $response = [
                "result" => "No Videos to show"
            ];

            return response($response, 201);
            
        }
    }
}

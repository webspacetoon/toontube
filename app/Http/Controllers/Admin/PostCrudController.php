<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PostRequest;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD; //
use Backpack\CRUD\app\Models\Traits\CrudTrait;


/**
 * Class PostCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PostCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    use CrudTrait;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Post::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/post');
        CRUD::setEntityNameStrings('post', 'posts');

        $this->crud->allowAccess('show');
        $this->crud->enableExportButtons();

        $this->crud->addFilter([
            'name'  => 'category_id',
            'type'  => 'select2',
            'label' => 'Category'
        ], function () {
            return \App\Models\Category::all()
                ->pluck('name', 'id')
                ->toArray();
        }, function ($value) {
            return $this->crud
                ->query
                ->whereHas('category', function ($q) use ($value) {
                    $q->where('category_id', $value);
                });
        });
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name'  => 'title',
            'label' => 'title',
            'type'  => 'text',
        ]);
        $this->crud->addColumn([
            'name'     => 'Category',
            'label'    => 'Category',
            'type'     => 'relationship',
            'function' => function ($entry) {
                return  $entry->name;
            },
            'wrapper' => [
                'class' => 'form-group col-md-6'
            ]
        ]);
        $this->crud->addColumn([
            'name'     => 'user_id',
            'label'    => 'User',
            'type'     => 'relationship',
            'function' => function ($entry) {
                return  $entry->name;
            },
            'wrapper' => [
                'class' => 'form-group col-md-6'
            ]
        ]);
        $this->crud->addColumn([
            'name'         => "image",
            'label'        => "Post Image",
            'type'         => 'image',
            'aspect_ratio' => 1,
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PostRequest::class);
        $this->crud->removeField('user_id');
    }

    protected function setupShowOperation()
    {

        $this->crud->set('show.setFromDb', false);

        $this->crud->addColumns(['category_id', 'title', 'user_id', 'body']);

        $this->crud->addColumn(
            [
                'name'  => 'image',
                'label' => 'Image',
                'type'  => 'image',
            ],
        );
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

}

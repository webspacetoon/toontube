<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RateRequest;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

use App\Models\Rate;

/**
 * Class RateCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class RateCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;

    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkCloneOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Rate::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/rate');
        CRUD::setEntityNameStrings('rate', 'rates');


        $this->crud->allowAccess('show');
        $this->crud->enableExportButtons();

        $this->crud->addFilter(
            [
                'name'  => 'rate',
                'type'  => 'select2',
                'label' => 'Status'
            ],
            function () {
                return Rate::select('rate')
                    ->distinct()
                    ->get()
                    ->pluck('rate', 'rate')
                    ->toArray();
            },
            function ($value) {
                $this->crud->addClause('where', 'rate', $value);
            }
        );
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn('rate');
        
        $this->crud->addColumn([
            'name'     => 'Post',
            'label'    => 'Post',
            'type'     => 'relationship',
            'function' => function ($entry) {
                return  $entry->name;
            }
        ]);
        $this->crud->addColumn([
            'name'     => 'User',
            'label'    => 'User',
            'type'     => 'relationship',
            'function' => function ($entry) {
                return  $entry->name;
            }
        ]);
    }


    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);
        $this->crud->addColumns(['post_id', 'user_id', 'rate']);
    }


    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(RateRequest::class);

        CRUD::setFromDb();
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    private function getColumnsData($show = FALSE)
    {
        return [

            [
                'name'     => 'Post',
                'label'    => 'Post',
                'type'     => 'relationship',
                'function' => function ($entry) {
                    return  $entry->name;
                }
            ],
            [
                'name'     => 'User',
                'label'    => 'User',
                'type'     => 'relationship',
                'function' => function ($entry) {
                    return  $entry->name;
                }
            ],
        ];
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CommentRequest;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;


/**
 * Class CommentCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */

class CommentCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;

    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkCloneOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */

    public function setup()
    {
        CRUD::setModel(\App\Models\Comment::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/comment');
        CRUD::setEntityNameStrings('comment', 'comments');

        $this->crud->allowAccess('show');
        $this->crud->enableExportButtons();

        $this->crud->addFilter([
            'name'  => 'post_id',
            'type'  => 'select2',
            'label' => 'Post'
        ], function () {
            return \App\Models\Post::all()
                ->pluck('title', 'id')
                ->toArray();
        }, function ($value) {
            return $this->crud
                ->query
                ->whereHas('post', function ($q) use ($value) {
                    $q->where('post_id', $value);
                });
        });

        $this->crud->addFilter(
            [
                'name'  => 'user_id',
                'type'  => 'select2',
                'label' => 'User'
            ],
            function () {
                return \App\Models\User::all()->pluck('name', 'id')->toArray();
            },
            function ($value) {
                return $this->crud
                    ->query
                    ->whereHas('user', function ($q) use ($value) {
                        $q->where('user_id', $value);
                    });
            }
        );
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */

    protected function setupListOperation()
    {
        
        $this->crud->addColumn([
            'name'     => 'Post',
            'label'    => 'Post',
            'type'     => 'relationship',
            'function' => function ($entry) {
                return  $entry->name;
            }
        ]);
        $this->crud->addColumn([
            'name'     => 'User',
            'label'    => 'user',
            'type'     => 'relationship',
            'function' => function ($entry) {
                return  $entry->name;
            }
        ]);
        $this->crud->addColumn([
            'name'  => 'body',
            'label' => 'Body',
            'type'  => 'text',
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */

    protected function setupCreateOperation()
    {
        CRUD::setValidation(CommentRequest::class);
        CRUD::setFromDb();
        $this->crud->removeField('user_id');
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);
        $this->crud->addColumns(['post_id', 'user_id', 'body']);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    private function getColumnsData($show = FALSE)
    {
        return [

            [
                'name'     => 'Post',
                'label'    => 'Post',
                'type'     => 'relationship',
                'function' => function ($entry) {
                    return  $entry->name;
                }
            ],
            [
                'name'     => 'User',
                'label'    => 'user',
                'type'     => 'relationship',
                'function' => function ($entry) {
                    return  $entry->name;
                }
            ],
            [
                'name'  => 'body',
                'label' => 'Body',
                'type'  => 'text',
            ],
        ];
    }
}

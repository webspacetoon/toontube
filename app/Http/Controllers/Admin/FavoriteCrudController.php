<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\FavoriteRequest;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class FavoriteCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */

class FavoriteCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkCloneOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */

    public function setup()
    {
        CRUD::setModel(\App\Models\Favorite::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/favorite');
        CRUD::setEntityNameStrings('favorite', 'favorites');

        $this->crud->allowAccess('show');
        $this->crud->enableExportButtons();
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */

    protected function setupListOperation()
    {
        
        $this->crud->addColumn([
            'name'     => 'Post',
            'label'    => 'Post',
            'type'     => 'relationship',
            'function' => function ($entry) {
                return  $entry->name;
            }
        ]);
        $this->crud->addColumn([
            'name'     => 'User',
            'label'    => 'User',
            'type'     => 'relationship',
            'function' => function ($entry) {
                return  $entry->name;
            }
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */

    protected function setupCreateOperation()
    {
        CRUD::setValidation(FavoriteRequest::class);
        CRUD::setFromDb();
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    private function getColumnsData($show = FALSE)
    {
        return [

            [
                'name'     => 'Post',
                'label'    => 'Post',
                'type'     => 'relationship',
                'function' => function ($entry) {
                    return  $entry->name;
                }
            ],
            [
                'name'     => 'User',
                'label'    => 'User',
                'type'     => 'relationship',
                'function' => function ($entry) {
                    return  $entry->name;
                }
            ],
        ];
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\VideoRequest;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;


/**
 * Class VideoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class VideoCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Video::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/video');
        CRUD::setEntityNameStrings('video', 'videos');

        $this->crud->addFields($this->getFieldsData(TRUE));



        $this->crud->addField([
            'name'  => 'URL',
            'label' => 'URL',
            'type'  => 'video',
        ]);
        $this->crud->addField([
            'name'  => 'title',
            'label' => 'Title',
            'type'  => 'text'
        ]);
        $this->crud->addField([
            'name'      => 'series_id',
            'label'     => 'Series',
            'type'      => 'select',
            'entity'    => 'series',
            'attribute' => 'name',
            'model'     => 'App\Models\Serie',
        ]);
        $this->crud->addField([
            'name'    => 'intro_start',
            'label'   => 'intro_start',
            'type'    => 'time',
            'wrapper' => [
                'class'      => 'form-group col-md-6'
            ],
        ]);
        $this->crud->addField([
            'name'      => 'intro_end',
            'label'     => 'intro_end',
            'type'      => 'time',
            'wrapper'   => [
                'class' => 'form-group col-md-6'
            ],
        ]);
        $this->crud->addField([
            'name'      => 'outro_start',
            'label'     => 'outro_start',
            'type'      => 'time',
            'wrapper'   => [
                'class' => 'form-group col-md-6'
            ],
        ]);
        $this->crud->addField([
            'name'    => 'outro_end',
            'label'   => 'outro_end',
            'type'    => 'time',
            'wrapper' => [
                'class'      => 'form-group col-md-6'
            ],
        ]);


        $this->crud->allowAccess('show');
        $this->crud->enableExportButtons();

        $this->crud->addFilter([
            'name'  => 'series_id',
            'type'  => 'select2',
            'label' => 'Serie'
        ], function () {
            return \App\Models\Serie::all()->pluck('name', 'id')->toArray();
        }, function ($value) {
            return $this->crud->query->whereHas('series', function ($q) use ($value) {
                $q->where('series_id', $value);
            });
        });
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {

        CRUD::addColumns([
            'title',
            'series_id',
            'URL',
            'intro_start',
            'intro_end',
            'outro_start',
            'outro_end',
        ]);

        // $this->crud->addColumns($this->getColumnsData(TRUE));

        $this->crud->addColumn([
            'name'     => 'series_id',
            'label'    => 'series',
            'type'     => 'relationship',
            'function' => function ($entry) {
                return  $entry->name;
            }
        ]);
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);
        $this->crud->addColumns([
            'title',
            'series_id',
            'URL',
            'intro_start',
            'intro_end',
            'outro_start',
            'outro_end'
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(VideoRequest::class);

        CRUD::setFromDb();
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SerieRequest;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;


/**
 * Class SerieCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SerieCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;


    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;


    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkCloneOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Serie::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/serie');
        CRUD::setEntityNameStrings('serie', 'series');

        $this->crud->allowAccess('show');
        $this->crud->enableExportButtons();

        $this->crud->addField([
            'name'  => 'name',
            'label' => 'Title',
            'type'  => 'text',
        ]);
        $this->crud->addField([
            'label'     => "Category",
            'type'      => 'select',
            'name'      => 'category_id',
            'entity'    => 'category',
            'attribute' => 'name',
            'model'     => 'App\Models\Category',
        ]);

        $this->crud->addFilter([
            'name'  => 'category_id',
            'type'  => 'select2',
            'label' => 'Category'
        ], function () {
            return \App\Models\Category::all()
                ->pluck('name', 'id')
                ->toArray();
        }, function ($value) {
            return $this->crud
                ->query->whereHas('category', function ($q) use ($value) {
                    $q->where('category_id', $value);
                });
        });
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // $this->crud->addColumns($this->getColumnsData(TRUE));

        $this->crud->addColumn([
            'name'  => 'name',
            'label' => 'Title',
            'type'  => 'text',
        ]);
        $this->crud->addColumn([
            'name'     => 'category',
            'label'    => 'Category',
            'type'     => 'relationship',
            'function' => function ($entry) {
                return  $entry->name;
            }
        ]);
        
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(SerieRequest::class);

        CRUD::setFromDb();
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);
        $this->crud->addColumns(['name', 'category_id']);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

}

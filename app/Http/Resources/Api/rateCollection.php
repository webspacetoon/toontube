<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\ResourceCollection;

class rateCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            // 'id' => $this->collection->keys(),
            'data' => $this->collection->values(),
            // 'user_id' => $this->collection,
            // 'rate' => $this->collection->keys(),
            'links' => [
                'self' => 'link-value',
            ],
        ];
    }
}
